let axios = require('axios');
let BASE_URL = 'https://www.googleapis.com/youtube/v3/search';

module.exports = function (options, callback) {
  let params = {
    part: 'snippet',
    q: (options.term) ? options.term : 'Test',
    maxResults: (options.items) ? options.items : 5,
    type: 'video'
  };
  
  axios.get('api/search/', { params }  )
  .then(response => {
    if (callback) { callback(response.data) }
  })
  .catch(error => console.error(error));

}