require('./bootstrap');
import Element from 'element-ui'
import VueRouter from 'vue-router';
import YoutubeList from './components/List.vue'
import SingleVideoDetail from './components/SingleVideoPlayer.vue'

window.Vue = require('vue');
window.eventBus = new Vue({});

Vue.use(Element);
Vue.use(VueRouter);

const routes = [
    {path: '/', component: YoutubeList, 'name': 'youtube-list'},
    {path: '/video/:id', component: SingleVideoDetail, 'name': 'youtube-video'}
];

const router = new VueRouter({
    routes
});

const app = new Vue({
    router
}).$mount('#app');