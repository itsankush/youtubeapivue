<?php

namespace App\Http\Controllers;
use App\Youtube;
use Illuminate\Http\Request;

class YoutubeController extends Controller
{
    public function searchVideos(Request $request)
    {
        $query = $request->query('q');
        $youtube= new Youtube(config('youtube.key'));
        $result= $youtube->searchVideos($query);
        return $result;
    }
   
}
